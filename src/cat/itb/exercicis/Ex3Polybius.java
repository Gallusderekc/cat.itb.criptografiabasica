package cat.itb.exercicis;

import java.util.Scanner;

public class Ex3Polybius {

    static void encrypter(String plainText)
    {
        plainText=plainText.toUpperCase();

        char allchar[][] = {
                {'A', 'B', 'C', 'Ç', 'D', 'E', 'F'},
                {'G', 'H', 'I', 'J', 'K', 'L', 'M'},
                {'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S'},
                {'T', 'U', 'V', 'W', 'X', 'Y', 'Z'},
                {'0', '1', '2', '3', '4', '5', '6'},
                {'7', '8', '9', '-', '+', '*', '/'},
                {'.', ',', ' ', '?', '¿', '!', '¡'}};
        int len=plainText.length();
        char b;
        for(int i=0; i<len; i++) {
            b=plainText.charAt(i);
            for(int j=0; j<7; j++) {
                for(int k=0; k<7; k++) {
                    if(b==allchar[j][k]) {
                        int f=j+1;
                        int s=k+1;

                        System.out.print(f+""+s+" ");
                        System.out.print("\t");
                    }
                }
            }
        }
    }

    static void decrypter(String plainText)
    {
        plainText=plainText.toUpperCase();

        char allchar[][] = {
                {'A', 'B', 'C', 'Ç', 'D', 'E', 'F'},
                {'G', 'H', 'I', 'J', 'K', 'L', 'M'},
                {'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S'},
                {'T', 'U', 'V', 'W', 'X', 'Y', 'Z'},
                {'0', '1', '2', '3', '4', '5', '6'},
                {'7', '8', '9', '-', '+', '*', '/'},
                {'.', ',', ' ', '?', '¿', '!', '¡'}};
        int len=plainText.length();
        char b;
        for(int i=0; i<len; i++) {
            b=plainText.charAt(i);
            for(int j=0; j<7; j++) {
                for(int k=0; k<7; k++) {
                    if(allchar[j][k]==b) {
                        int f=j+1;
                        int s=k+1;

                        System.out.print(j+""+k+" ");
                        System.out.print("\t");
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        String plainText = "Bona tarda i bona sort";
        encrypter(plainText);
        System.out.println();
        String plainText1 = "11";
        decrypter(plainText1);
    }
}
