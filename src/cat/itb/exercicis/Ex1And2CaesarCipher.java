package cat.itb.exercicis;

public class Ex1And2CaesarCipher {

    /*
    https://www.youtube.com/watch?v=44ADNK7ggkA
    https://www.google.com/search?q=how+to+create+caesar+algorithm+java&biw=2013&bih=1044&ei=kWHcYcKMFKmW9u8P3tG5iAo&ved=0ahUKEwiCmY_-z6f1AhUpi_0HHd5oDqEQ4dUDCA4&uact=5&oq=how+to+create+caesar+algorithm+java&gs_lcp=Cgdnd3Mtd2l6EAMyBggAEBYQHjIGCAAQFhAeOgQIABBHSgQIQRgASgQIRhgAULkOWO0TYKkVaABwA3gAgAFqiAHlA5IBAzQuMZgBAKABAcgBCMABAQ&sclient=gws-wiz
     */
/*    public static String encrypt(String plaintext, int shift) {
        if (shift > 26) {
            shift = shift % 26;
        } else if (shift < 0) {
            shift = (shift % 26) + 26;
        }
        String cipherText = "";
        int length = plaintext.length();
        for (int i = 0; i < length; i++) {
            char ch = plaintext.charAt(i);
            if (Character.isLetter(ch)) {
                if (Character.isLowerCase(ch)) {
                    char c = (char) (ch + shift);
                    if (c > 'z') {
                        cipherText += (char) (ch - (26 - shift));
                    } else {
                        cipherText += c;
                    }
                } else if (Character.isUpperCase((ch))) {
                    char c = (char) (ch + shift);
                    if (c > 'Z') {
                        cipherText += (char) (ch - (26 - shift));
                    } else {
                        cipherText += c;
                    }
                }
            } else {
                cipherText += ch;
            }
        }
        return cipherText;
    }*/

    public static String decrypt(String plaintext, int shift) {
        if (shift > 26) {
            shift = shift % 26;
        } else if (shift < 0) {
            shift = (shift % 26) + 26;
        }
        String cipherText = "";
        int length = plaintext.length();
        for (int i = 0; i < length; i++) {
            char ch = plaintext.charAt(i);
            if (Character.isLetter(ch)) {
                if (Character.isLowerCase(ch)) {
                    char c = (char) (ch - shift);
                    if (c < 'a') {
                        cipherText += (char) (ch + (26 - shift));
                    } else {
                        cipherText += c;
                    }
                } else if (Character.isUpperCase((ch))) {
                    char c = (char) (ch - shift);
                    if (c < 'A') {
                        cipherText += (char) (ch + (26 - shift));
                    } else {
                        cipherText += c;
                    }
                }
            } else {
                cipherText += ch;
            }
        }
        return cipherText;
    }

    public static void main(String[] args) {
        String text = "Xfux vzn jx Otj Gnijs?";
        String decrypted = decrypt(text, 5);
        System.out.println("Exercici 1:");
        System.out.println(decrypted);

/*        String text2 = "Si, es el president dels Estats Units d'America";
        String cipher = encrypt(text2, 5);
        System.out.println(cipher);*/

        String text3 = "CEBTENZZVAT, ZBGURESHPXRE";
        String decrypted2 = decrypt(text3, 13);
        System.out.println("Exercici 2:");
        System.out.println(decrypted2);
    }
}
